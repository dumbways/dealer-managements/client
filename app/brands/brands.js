let brands = {
    init() {
        $.get(`${VAR.baseUrl}brands`).done(function(res) {
            if(res.status) {
                res = res.data
                eaControl.render(`app/brands/brands.list`, {brands: res}, '#eaContent', function () {
                    $('#table-list-brands').DataTable()
                    brands.event(res)
                })
            } else {
                eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
            }
        })
    },
    event(res) {
        let editElement = null

        $('#table-list-brands').on('click', '[aria-action="delete-brands"]', function() {
            let brands_id = $(this).data('id')
            eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                '<h6 class="message">Are you sure you want to delete this brands ?</h6>', function() {
                /** --------------- Do action here --------------- */
                $.ajax({ url: `${VAR.baseUrl}brands/${brands_id}`, method: 'DELETE' }).done(function(res) {
                    if (res.status) {
                        brands.init()
                        eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Brands was successfully deleted.</h6>')
                    } else {
                        eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                    }
                })
            })
        })

        $('#table-list-brands').on('click', '[aria-action="edit-brands"]', function() {
            $('[container="form-subject"]').html('Edit')
            $('[aria-action="cancel-edit-brands"]').fadeIn()

            let brands_id = $(this).data('id')
            for (let i = 0; i < res.length; i++) {
                let row = res[i]
                if(row.id == brands_id) { editElement = row }
            }

            $('[name="brand_name"]').val(editElement.brand_name)
            $('[name="brand_name"]').focus()
        })

        $('[aria-action="cancel-edit-brands"]').click(function() {
            $('[container="form-subject"]').html('Add')
            $('[aria-action="cancel-edit-brands"]').fadeOut()
            $('[name="brand_name"]').val('')
            $('[name="brand_name"]').focus()
            editElement = null
        })

        const brandsValidator = $('#form-brands').validate({
            rules       : { "brand_name": "required" },
            messages    : { "brand_name": "Name is required" },
            invalidHandler: function (event, brandsValidator) {
                const errors = brandsValidator.numberOfInvalids()
                if (errors) {
                    $('#form-brands-card-error .alert').fadeIn("fast")
                }
            },
            errorContainer: '#form-brands-card-error .alert',
            errorLabelContainer: '#formBrandsErrorMessage',
            wrapper: 'span',
            submitHandler: function (form) {
                /** Form brands */
                const formData = $(form).serialize()
                $('[aria-action="submit-brands"]').attr('disabled', true)
                if(!editElement) {
                    $.ajax({ url: `${VAR.baseUrl}brands`, method: 'POST', data: formData }).done(function(res) {
                        if (res.status) {
                            brands.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Brands was successfully added.</h6>')
                        } else {
                            brandsValidator.showErrors({'brand_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-brands"]').attr('disabled', false)
                    })
                } else {
                    $.ajax({ url: `${VAR.baseUrl}brands/${editElement.id}`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            brands.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Brands was successfully edited.</h6>')
                        } else {
                            brandsValidator.showErrors({'brand_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-brands"]').attr('disabled', false)
                    })
                }
                return false
            }
        })
    }
}