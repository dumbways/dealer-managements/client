(function() {
	'use strict';
	
	const app = {
		renderMenu(data) { 
			$(window).scrollTop(0);
			eaControl.render('app/other/side_menu', data, '#eaNavMenuPanel', function () {
				require(['overlayscrollbars'], function () {
					$(document).ready(function() {
						$(".scrollme").overlayScrollbars({ overflowBehavior : { x : "hidden", y : "scroll" } }).overlayScrollbars();
					})
					$(".nav-link[data-toogle=collapse]").on("click", function() {
						let mode = $(this).attr("aria-expanded");
						if (mode == 'true') {
							$(this).siblings(".collapse").slideUp('fast');
							mode = false;
						} else {
							$(this).siblings(".collapse").slideDown('fast');
							mode = true;
						}
						$(this).attr("aria-expanded", mode);
					})

					/** Active Menu */
					let setActiveMenu = (hash = '') => {
						if (!hash) {
							hash = window.location.hash
						}
						hash = hash.split('/')
						let navbar = $('.ea-navbar')
						navbar.find('.active').removeClass('active')
						navbar.find('.collapse.show').removeClass('show')
						navbar.find('[aria-expanded="true"]').attr('aria-expanded',false)

						let currentHash = '#';
						$.each(hash, function (i, val) {
							if (val != '#') {
								currentHash += '/' + val;
								let navLink = navbar.find('.nav-link[href="'+currentHash+'"]');
								let parentCollapse = navLink.parents('.collapse');
	
								navLink.addClass('active')
								if (parentCollapse) {
									parentCollapse.addClass('show')
									parentCollapse.attr('aria-expanded',true)
									parentCollapse.siblings().attr('aria-expanded',true)
								}
							}
						})
					}

					$(function () { setActiveMenu() })
					$(window).on('hashchange', function() { 
						setActiveMenu() 
						/** Remove Bug Select2 */
						$('body > .select2-container').remove()
					})
					$('.nav-link[href^="#"]').click(function(){ setActiveMenu($(this).attr('href')) })
				})
			})
		},
		editFramework() { 
			// if mobile detected auto toogled
			const window_with = $(window).width();
			if (window_with <= 535) {
				$('#eaHeader, #eaBody').toggleClass('toggled');
			}
		},
	};
	const fn = {
		routes() {
			eaControl.router.before('', function (context) {
				eaControl.removeSocket()
			})
			eaControl.router.get('#/donation', function (context) {
				require(["app.dashboard.personal", "jquery_validate", "dtbootstrap"], function () {
					dashboardPersonal.init()
				})
			})
			eaControl.router.get('#/customer', function (context) {
				require(["app.customer", "jquery_validate", "dtbootstrap"], function () {
					customer.init()
				})
			})
			eaControl.router.get('#/brands', function (context) {
				require(["app.brands", "jquery_validate", "dtbootstrap"], function () {
					brands.init()
				})
			})
			eaControl.router.get('#/cars', function (context) {
				require(["app.cars", "jquery_validate", "dtbootstrap", "bootstrap_select"], function () {
					cars.list.init()
				})
			})
			eaControl.router.get('#/cars/:carsid', function (context) {
				const carsid = this.params.carsid
				require(["app.cars", "jquery_validate", "dtbootstrap", "bootstrap_select"], function () {
					cars.view.init(carsid)
				})
			})
			eaControl.router.notFound = function (context) {
				console.log('not found')
				let defaultMenu = eaControl.userinfo.menu[0] ? eaControl.userinfo.menu[0].menu[0] ? eaControl.userinfo.menu[0].menu[0].url : '#/customer' : '#/customer'
				window.location = defaultMenu
				// let defaultMenu = '#/customer'
				// window.location = defaultMenu
			}
			eaControl.router.get('', function (context) {
				// let defaultMenu = '#/customer'
				let defaultMenu = eaControl.userinfo.menu[0] ? eaControl.userinfo.menu[0].menu[0] ? eaControl.userinfo.menu[0].menu[0].url : '#/customer' : '#/customer'
				console.log('default', eaControl.userinfo.menu[0])
				window.location = defaultMenu
			})
			eaControl.router.run()
		},
		showLogo(logo, source) {
			const img = document.createElement('img');

			img.src = source;
			img.setAttribute('class', 'view-logo');
			img.onload = function() { 
				$(`${logo}Link`).val(source);
				$(`${logo}Container`).removeClass('d-none');
				$(`${logo}Copy`).on('click', function(e) {
					e.preventDefault();
					const link = $(`${logo}Link`)[0];
					link.select();
					document.execCommand('copy');
					eaControl.popNotif('success', 'Logo path has been copied');
				});
				$(logo).html(img);
			};
		}
	};
	// for global controller
	const global = {
		scrollTop() {
			$('html,body').animate({ scrollTop: 0 }, 'slow');
		},
		dateFormat(val, format) {
			let date = new Date (Date.parse(val));
			let D = date.getDay()
			let d = date.getDate()
			let m = date.getMonth() + 1
			let M = VAR.month[date.getMonth() + 1]
			let Y = date.getFullYear()

			if (d < 10) { d = '0' + d}
			if (m < 10) { m = '0' + m}

			format = format.replace("D", D);
			format = format.replace("d", d);
			format = format.replace("m", m);
			format = format.replace("M", M);
			format = format.replace("Y", Y);

			return format;
		},
		currency(val) {
			let res = '';
			for (; val >= 1000; val = parseInt(val/1000)) {
				let mod = val % 1000;
				mod = ('00' + mod).slice(-3)
				res = res == '' ? mod : mod + '.' + res;
			}
			return 'Rp ' + val + '.' + res + ',00';
		},
		getQuerystring(key = null) {
			let location = window.location.href.split('?');
			let query = location[1] ? location[1].split('&') : []
			let res = {}
			query.forEach(function(val, key) {
				val = val.split("=");
				if (val[0] == key) { return val[1] }

				res[val[0]] = val[1]
			})

			return res;
		},
		isEditable(allowedRoles) {
			let isEditable = null
            eaControl.userinfo.role.forEach(function(userrole) {
				allowedRoles.forEach(function(allowedRole) {
					if(userrole.rolename == allowedRole) {
						isEditable = 1
					}
				})
			})
			return isEditable
		}
	}
	$(function() {
		window.eaControl = new core(function () {
			app.renderMenu(eaControl.userinfo);
			eaControl.fnGlobal = global;
			fn.routes();
			app.editFramework();
			// $('[data-toggle="tooltip"]').tooltip();
		});
		
		dust.helpers.findRoleApi = function (chunk, context, body, param) {
			const roles = dust.helpers.tap(param.key, chunk, context),
				roleid = dust.helpers.tap(param.value, chunk, context);
				
			let i = 0,
				found = false;
			
			if (roleid && roles && roles.length) {
				while (i < roles.length && !found) {
				if (roles[i].roleid === parseInt(roleid)) {
						found = true;
					}
					i++;
				}
			}
			return chunk.write(found ? 'selected="selected"' : '');
		};

	});		
})();