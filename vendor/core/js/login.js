'use strict';

(function() {
    const signIn = {
        checkError : () => {
            const hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

            if (window.location.href.indexOf('err=')) {
                let i = 0,
                    f = false,
                    err,errCode;

                while (i < hashes.length && !f) {
                    err     = hashes[i].split('#');
                    errCode = err[0].split('=');

                    if (errCode[1] === '401') {
                        signIn.showMessage(errCode[1], `<p class="mb-1">You are not authorized due to invalid credentials.</p>
                                                        <p>Make sure you enter the right credentials or please contact your administrator for assistance.</p>`);
                        f = true;
                    }
                    else if (errCode[1] === '403') {
                        signIn.showMessage(errCode[1], `Your access has been denied.`);
                        f = true;
                    }

                    i++;
                }

                signIn.clearCookies();
            }
        },

        authenticate : async () => {
            const data = $('#en-form-signin').serializeArray();
            const auth = {
                login    : data[0].value,
                password : data[1].value,
                atyp     : VAR.atyp
            };

            if(!auth.login && !auth.password) {
                signIn.showMessage('401', 'Unauthorized access');

                return;
            }

            $.ajax({
                url  : `${VAR.baseUrl}/authenticate`,
                type : 'POST',
                data : {
                    username: auth.login,
                    password: auth.password
                },
                dataType : `JSON`,
                success: (res) => {
                    // res = res[VAR.appName]
                    if(!res.status) {
                        signIn.showMessage(401, `Not Authorized.`);

                        return;
                    }

                    document.cookie = `token=${res.data};`;

                    window.location = 'index.html';
                },
                error: (xhr, status, error) => {
                    signIn.showMessage(xhr.status, error);

                    $('#en-btn-signin > span').removeClass('loading');
                    $('#en-btn-signin > div.loading-container').addClass('d-none');
                }
            });
        },

        showMessage : (code, message) => {
            $('#enSignInMessage').removeClass('d-none').html(message);

            $('#en-btn-signin > span').removeClass('loading');
            $('#en-btn-signin > div.loading-container').addClass('d-none');
        },

        clearCookies : () => {
            const cookies = document.cookie.split(";");
                
            for (let i=0; i<cookies.length; i++) {
                let cookie = cookies[i],
                    eqPos  = cookie.indexOf("="),
                    name   = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;

                document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
            }
        }
    };

    $(function() {
        signIn.checkError();

        const
            date = new Date(),
            year = date.getFullYear(),
            src  = $(`span#year`).html();

        $(`span#year`).append(`${src !== year.toString() ? '-' + year : ''}`);

        $(`span#reveal-pass`).on(`mousedown`, function() {
            $('input[name="password"][type="password"]').addClass('d-none');
            $('input[name="password"][type="text"]').removeClass('d-none');
        }).on(`mouseup`, function() {
            $('input[name="password"][type="password"]').removeClass('d-none');
            $('input[name="password"][type="text"]').addClass('d-none');
        });

        $('input[name="password"][type="password"]').on('keyup', function() {
            $(`input[name="password"][type="text"]`).val($(this).val());
        });

        $('#en-btn-signin').on('click', function(e) {
            e.preventDefault();

            if($(this).find('> span').hasClass('loading')) {
                return;
            }

            $(this).find('> span').addClass('loading');
            $(this).find('> div.loading-container').removeClass('d-none');

            signIn.authenticate();
        });

        $('#en-form-signin').on('submit', function() {
            signIn.authenticate();
        });
    });
}());